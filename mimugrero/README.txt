MiMugrero is an application made in AngularJS

It has the following modules:

*Main page
*Catalog of items for sale and detail in modal window
*Shopping cart
*Registration of articles (administration panel module)

You can test the application with http-server.
You must have it installed on your computer,
You can review the instructions at:

https://www.npmjs.com/package/http-server

Then you have to go to the root folder and
indicate where the index.html file is located

Example on windows:

C:\Projects\my dump\my dump>http-server ./app

----------------------------------------------------------------------
Mi mugrero App es un aplicativo hecho en AngularJS 

Cuenta con los siguientes módulos:

*Pagina inicial 
*Catálogo de artículos en venta y detalle en ventana modal
*Carrito de compras
*Alta de artículos (módulo del panel de administración )

Puede probar el aplicativo con http-server. 
Debe tenerlo instalado en su ordenador, 
puede revisar las instrucciones en:

https://www.npmjs.com/package/http-server

Despues hay que situarse en la carpeta raiz e 
indicar donde se encuentra el archivo index.html

Ejemplo en windows: 

C:\Projects\mimugrero\mimugrero>http-server ./app

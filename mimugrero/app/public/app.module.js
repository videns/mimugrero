
var app = angular.module('mimugrero', [
  'ngAnimate',
  'ngRoute',
  'home',
  'admin',
  'catalog',
  'cart',
  'checkout'
]);

/*
*Inicializar el carrito con datos dummy
*calcular el indice actual de los artículos
*/
app.run(function ($rootScope, $http) {
  $rootScope.items = [];
  $rootScope.currentId = 0;
  $rootScope.currentIdInCart = 0;
  $rootScope.cart = [];
  $rootScope.subtotal = 0;
  $rootScope.subtotalIVA = 0;
  $rootScope.currentItemInModal = "";
  $rootScope.itemsInCart = 0;
  $rootScope.total = 0;

  if ($rootScope.items.length <= 0) {
    $http.get("data/data.json")
      .then(function (response) {
        $rootScope.items = response.data.data;
        console.log($rootScope.items)
        var maxId = 0;
        if ($rootScope.items.length == 1) {
          maxId = $rootScope.items[0].Id;
        }
        else {
          maxId = $rootScope.items[0].Id;

          angular.forEach($rootScope.items, function (value) {
            if (maxId < value.Id) {
              maxId = value.Id;
            }
          });
        }

        $rootScope.currentId = maxId;

      });
  }
});


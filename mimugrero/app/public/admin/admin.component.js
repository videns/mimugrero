angular.
  module('admin').
  component('admin', {
    templateUrl: 'admin/admin.template.html',
    controller: function AdminController($scope, $window, $rootScope) {

      $scope.item = {};

      $scope.addItem = function () {

        var filtered = $rootScope.items.filter(function (value) {
          return value.imdbID == $scope.item.imdbID;
        });

        if ($scope.item.imdbID == null || $scope.item.imdbID.trim() == "") {
          $window.alert("imdb cannot be empty!!");
          $scope.item = {}
          return;
        }

        if (filtered.length != 0) {
          $window.alert("The imdb movie are exists!! . Please add other imdb.");
          $scope.item = {}
        }
        else {
          var item = {
            Id: $scope.item.Id,
            ImgUrl: $scope.item.ImgUrl,
            Title: $scope.item.Title,
            Year: $scope.item.Year,
            imdbID: $scope.item.imdbID,
            Price: $scope.item.Price,
            Description: $scope.item.Description
          };

          $rootScope.currentId++;
          item.Id = $rootScope.currentId;
          $rootScope.items.push(item);
          $scope.item = {}
        }
      };

      $scope.deleteItem = function (id) {
        var w = $window.confirm('Are you sure you want to delete ?');
        if (w == true) {
          var filtered = $rootScope.items.filter(function (value) {
            return value.Id != id;
          });
          $rootScope.items = filtered;
        }
      };
    }
  });



angular.
  module('mimugrero').
  config(['$routeProvider',
    function config($routeProvider) {
      $routeProvider.
        when('/admin', {
          template: '<admin></admin>'
        }).
        when('/catalog', {
          template: '<catalog></catalog>'
        }).
        when('/cart', {
          template: '<cart></cart>'
        }).
        when('/checkout', {
          template: '<checkout></checkout>'
        }).
        when('/home', {
          template: '<home></home>'
        }).
        otherwise('/home');
    }
  ]);

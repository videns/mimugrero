var catalog = angular.module('catalog', []);

catalog.config([
  '$compileProvider',
  function ($compileProvider) {
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension):/);
  }
]);

catalog.directive('modalDialog', function ($templateCache, $compile, $http) {
  return {
    restrict: 'EA',
    scope: {
      show: '=',
      addocartm: '&',
      templateInfo: '@'
    },
    replace: true,
  
    link: function (scope, element, attrs) {
    console.log('entrando a la directiva')
      $http.get(
        scope.templateInfo, { cache: $templateCache })
        .then(function (tplContent) {

          element.replaceWith($compile(tplContent.data)(scope));
        });

      scope.dialogStyle = {};

      if (attrs.width) {
        scope.dialogStyle.width = attrs.width + '%';
        scope.dialogStyle.left = ((100 - attrs.width) / 2) + '%';
      }
      if (attrs.height) {
        scope.dialogStyle.height = attrs.height + '%';
        scope.dialogStyle.top = ((100 - attrs.height) / 2) + '%';
      }

      scope.hideModal = function () {
        scope.show = false;
      };

      scope.add = function () {
        scope.addocartm();
        scope.show = false;
      };

      scope.cancel = function () {
        scope.show = false;
      };
    }
  };
});

catalog.controller('itemsController', function ($scope, $rootScope) {

  $scope.modalShown = false;
  $scope.info = { name: "Info", item: "", itemsCount: 0 };

  $scope.toggleModal = function (i) {
    $rootScope.currentItemInModal = i;
    $scope.modalShown = !$scope.modalShown;
  };

  $scope.addToCart = function (i) {
    const found = $rootScope.cart.find(element => element.Id == i.Id);
    if(found) 
    {
      found.Quantity++;
    }
    else
    {  
      i.Quantity = 1;
      $rootScope.cart.push(i);
    }

    $rootScope.itemsInCart = 0;
    angular.forEach($rootScope.cart, function (value) {
        $rootScope.itemsInCart += value.Quantity;
      });
  }

  $scope.addocartm = function () {
    const found = $rootScope.cart.find(element => element.Id == $rootScope.currentItemInModal.Id);
    if(found)
    {
      found.Quantity++;
    }
    else
    {
      $rootScope.currentItemInModal.Quantity = 1;
      $rootScope.cart.push($rootScope.currentItemInModal)
    }

    $rootScope.itemsInCart = 0;
    angular.forEach($rootScope.cart, function (value) {
        $rootScope.itemsInCart += value.Quantity;
      });

  }
});

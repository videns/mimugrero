angular.
    module('checkout').
    component('checkout', {
        templateUrl: 'checkout/checkout.template.html',
        controller: function CheckoutController($rootScope, $scope) {

            $scope.total = 0
            $scope.total = $rootScope.total;

            $rootScope.cart = [];
            $rootScope.itemsInCart = 0;
            $rootScope.total = 0;
            $rootScope.subtotal = 0;
            $rootScope.subtotalIVA = 0;
        }
    });



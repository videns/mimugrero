angular.
    module('cart').
    component('cart', {
        templateUrl: 'cart/cart.template.html',
        controller: function CartController($scope, $rootScope, $window) {
            $scope.data = {}
            var getTotalSale = function () {
                $rootScope.subtotalIVA = 0;
                $rootScope.total = 0;
                $rootScope.subtotal = 0;

                angular.forEach($rootScope.cart, function (value) {
                    $rootScope.subtotal += value.Price * value.Quantity;
                });

                $rootScope.subtotalIVA = Number(($rootScope.subtotal * 0.16).toFixed(2));
                $rootScope.total = Number(($rootScope.subtotal * 1.16).toFixed(2));
            };

            getTotalSale();

            $scope.deleteItemFromCart = function (id) {
                var w = $window.confirm('Are you sure you want to delete ?');
                if (w == true) {
                    var filtered = $rootScope.cart.filter(function (value) {
                        return value.Id != id;
                    });

                    $rootScope.cart = filtered;
                    $rootScope.itemsInCart = 0;
                    angular.forEach($rootScope.cart, function (value) {
                        $rootScope.itemsInCart += value.Quantity;
                    });

                    getTotalSale();
                }
            };
        }
    });


